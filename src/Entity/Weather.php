<?php

namespace App\Entity;

use App\Repository\WeatherRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeatherRepository::class)
 */
class Weather
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $jsonValue;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="datetime")
     */
    private $sunrise;

    /**
     * @ORM\Column(type="float")
     */
    private $visibility;

    /**
     * @ORM\Column(type="float")
     */
    private $cloudcover;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\Column(type="float")
     */
    private $sealevelpressure;

    /**
     * @ORM\Column(type="datetime")
     */
    private $sunset;

    /**
     * @ORM\Column(type="float")
     */
    private $humidity;

    /**
     * @ORM\Column(type="float")
     */
    private $wgust;

    /**
     * @ORM\Column(type="float")
     */
    private $windchill;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJsonValue(): ?string
    {
        return $this->jsonValue;
    }

    public function setJsonValue(string $jsonValue): self
    {
        $this->jsonValue = $jsonValue;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getSunrise(): ?\DateTimeInterface
    {
        return $this->sunrise;
    }

    public function setSunrise(\DateTimeInterface $sunrise): self
    {
        $this->sunrise = $sunrise;

        return $this;
    }

    public function getVisibility(): ?float
    {
        return $this->visibility;
    }

    public function setVisibility(float $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }

    public function getCloudcover(): ?float
    {
        return $this->cloudcover;
    }

    public function setCloudcover(float $cloudcover): self
    {
        $this->cloudcover = $cloudcover;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getSealevelpressure(): ?float
    {
        return $this->sealevelpressure;
    }

    public function setSealevelpressure(float $sealevelpressure): self
    {
        $this->sealevelpressure = $sealevelpressure;

        return $this;
    }

    public function getSunset(): ?\DateTimeInterface
    {
        return $this->sunset;
    }

    public function setSunset(\DateTimeInterface $sunset): self
    {
        $this->sunset = $sunset;

        return $this;
    }

    public function getHumidity(): ?float
    {
        return $this->humidity;
    }

    public function setHumidity(float $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    public function getWgust(): ?float
    {
        return $this->wgust;
    }

    public function setWgust(float $wgust): self
    {
        $this->wgust = $wgust;

        return $this;
    }

    public function getWindchill(): ?float
    {
        return $this->windchill;
    }

    public function setWindchill(float $windchill): self
    {
        $this->windchill = $windchill;

        return $this;
    }
}
