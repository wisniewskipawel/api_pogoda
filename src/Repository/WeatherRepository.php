<?php

namespace App\Repository;

use App\Entity\Weather;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\Config\Definition\Exception\Exception;
/**
 * @method Weather|null find($id, $lockMode = null, $lockVersion = null)
 * @method Weather|null findOneBy(array $criteria, array $orderBy = null)
 * @method Weather[]    findAll()
 * @method Weather[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Weather::class);
    }
    
    public function saveJsonWeather(string $value)
    {
        $data = json_decode($value);
        $weather = new Weather();
        foreach($data->locations as $values)
        {
            $weather->setLocation($values->id);
            $weather->setAddress($values->address);
            $weather->setSunrise(new \DateTime($values->currentConditions->sunrise));
            $weather->setVisibility($values->currentConditions->visibility ? $values->currentConditions->visibility : 0);
            $weather->setCloudcover($values->currentConditions->cloudcover ? $values->currentConditions->cloudcover : 0);
            $weather->setDatetime(new \DateTime($values->currentConditions->datetime));
            $weather->setSealevelpressure($values->currentConditions->sealevelpressure ? $values->currentConditions->sealevelpressure : 0);
            $weather->setSunset(new \DateTime($values->currentConditions->sunset));
            $weather->setHumidity($values->currentConditions->humidity);
            $weather->setWgust($values->currentConditions->wgust ? $values->currentConditions->wgust : 0);
            $weather->setWindchill($values->currentConditions->windchill ? $values->currentConditions->windchill : 0);            
        }
        $weather->setJsonValue($value);
        $this->_em->persist($weather);
        $this->_em->flush();
    }
    
    public function searchAverage($initial, $final)
    {
        return $this->createQueryBuilder('w')
            ->select('w.location,w.datetime,w.visibility,w.cloudcover')
            ->Where('w.datetime > :initial')
            ->andWhere('w.datetime > :final')
            ->setParameter('initial', $initial)
            ->setParameter('final', $final)
            ->orderBy('w.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Weather[] Returns an array of Weather objects
    //  */
    /*
    public function findByExampleField($value)
    {            
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Weather
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
