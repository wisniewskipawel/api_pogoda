<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\WeatherRepository;
use App\Service\Calculations;



/**
* @Route("/json")
*/
class JsonController extends AbstractController
{
    
    private $weatherRepository;
    
    public function __construct(WeatherRepository $weatherRepository)
    {
        $this->weatherRepository = $weatherRepository;
    }
    
    /**
     * @Route("/{date}", name="json")
     */
    public function index($date,Calculations $calculations)
    {
        $result = $this->weatherRepository->searchAverage($date,date('Y-m-d'));
    
        return $this->json(['result' =>$result]);
    }
}
