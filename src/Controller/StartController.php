<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ApiWeather;
use App\Repository\WeatherRepository;

/**
 * @Route("/")
 */
class StartController extends AbstractController
{
    private $weatherRepository;
    private $array = ['Gdańsk','Szczecin','Zakopane','Wrocław','Opole','Częstochowa','Kielce','Kraków','Zakopane'];
    private $weather;
    
    public function __construct(WeatherRepository $weatherRepository,ApiWeather $weather)
    {
        $this->weatherRepository = $weatherRepository;
        $this->weather = $weather;
    }
    
    /**
     * @Route(name="start")
     */
    public function index(): Response
    {

        foreach ($this->array as $value)
        {
            $this->weatherRepository->saveJsonWeather($this->weather->getWeather($value));
        }
        return $this->render('start/index.html.twig', [
            'controller_name' => 'StartController',
        ]);
    }
}
