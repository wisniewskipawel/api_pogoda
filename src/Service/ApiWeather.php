<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

class ApiWeather {
    
    private $logger;
    
    private const api_url = 'https://visual-crossing-weather.p.rapidapi.com/forecast?aggregateHours=24&location=';
    private const api_location = 'Warszawa';
    private const contentType ='&contentType=json&shortColumnNames=0';
    
    private const api_key = 'b5083c5427msh4ffa138f8576a03p18ca8ajsnfab16cb4aa03';
    
    private const api_rapid_host = 'visual-crossing-weather.p.rapidapi.com';
    
    private $location = null;
    
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;        
    }
    
    public function getWeather(string $location)
    {
        $curl = curl_init();
        if(!$location)
        {
           $location = self::api_location;
        }
        
        curl_setopt_array($curl, [
        CURLOPT_URL => self::api_url.$location.self::contentType, 
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => [
                    "x-rapidapi-host: visual-crossing-weather.p.rapidapi.com",
                    "x-rapidapi-key: b5083c5427msh4ffa138f8576a03p18ca8ajsnfab16cb4aa03"
                ],
            ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
        throw new Exception("cURL Error #:" . $err);
        } 
        return $response;
    }
}

?>