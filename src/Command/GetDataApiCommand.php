<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Repository\WeatherRepository;
use App\Service\ApiWeather;

class GetDataApiCommand extends Command
{
    protected static $defaultName = 'app:get-data-api';
    protected static $defaultDescription = 'Retrieves weather data from a given table of Polish cities';
    
    private $array = ['Gdańsk','Szczecin','Zakopane','Wrocław','Opole','Częstochowa','Kielce','Kraków','Zakopane'];

    private $weatherRepository;
    private $weather;
    
    public function __construct(WeatherRepository $weatherRepository,ApiWeather $weather)
    {
        $this->weatherRepository = $weatherRepository;
        $this->weather = $weather;
        parent::__construct();
    }

    
    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Retrieves weather data from a given table of Polish cities')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Retrieves weather data from a given table of Polish cities')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        foreach ($this->array as $value)
        {
            $this->weatherRepository->saveJsonWeather($this->weather->getWeather($value));
        }
        
        $io->success('I have finished downloading data from the server.');

        return Command::SUCCESS;
    }
}
